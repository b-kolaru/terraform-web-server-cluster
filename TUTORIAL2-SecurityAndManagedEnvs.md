# Terraform Deployment to AWS with GitLab Security Policy Approval Rules and Managed DevOps Environments

## Known Working Version Details

Tested Date: **2023-08-08**

Testing Version (GitLab and Runner): **Enterprise Edition 16.4.0 SaaS**

Report Problems with New Issues here: https://gitlab.com/guided-explorations/aws/terraform/terraform-web-server-cluster/-/issues

There is nothing here that depends on GitLab.com, so it should work fine on other Gitlab instances that are licensed for Ultimate.

**IMPORTANT:** Requirements for self-paced execution of this tutorial are covered in [TUTORIAL-Requirements.md](TUTORIAL-Requirements.md)

## Visual Overview

![terraformtutorial2overview](images/terraformtutorial2overview.png)

## Conventions and Requirements

- `[ONLY FOR LEARNING]` = This tag means that for the sake of learning simplicity we doing or showing something that would not be typical in a production environment.
- These labs require that you have completed the steps in the tutorial [Serverless Framework Deployment from GitLab to AWS with Security Scanning](TUTUTORIAL.md)
- The labs do not direct you to watch the creation and destruction of the Lambda functions, applications, API gateway, etc - but you can feel free to watch these events to whatever degree you are familiar with the AWS console.

## Concepts To Watch For

- Many CI systems use ‘build failure’ on shared branches as the primary or exclusive way to force software defects to be dealt with. 
  - There are a number of downsides to this approach if other controls are possible - especially with security vulnerability defects. For instance, having to wait on assistance before any more work can be done. Also, the pain of one-by-one-failures discovery of vulnerabilities.
  - GitLab has very flexible approvals - some of which only become required under the right conditions. Since GitLabs Merge Request process supports this for security vulnerabilities, it prevents a the very disruptive workflow of failing pipelines on vulnerabilities, but still does not allow them to be merged into the next branch. This maximizes developer productivity.
- This tutorial shows the power of GitLab Security Policy approval rules and how they prevent code changes from introducing new vulnerabilities that are not in alignment with your security policies.
- `[ONLY FOR LEARNING]` Remember from the previous tutorial that in the real world (not our test project) - only vulnerabilities that we introduced with our Merge Request code changes would be in the merge request. As we proceed, we’ll be imagining the our own coding activities in our branch are what introduced all the found vulnerabilities.
- You will configure a security policy that will require a new merge approval - in production this would generally be someone from security. The security rule is configured after the findings are found so you can plainly see how this control works.
- Then, as the developer, you will remove the vulnerability instead of having to face the security team and ask for an exception for the critical vulnerability you accidentally introduced in your code. Once the vulnerability is removed, then new approval rule becomes optional.
- Notice that Security professionals only need to review the code and give approval if there is an attempt to merge the vulnerability - otherwise if the developer self-remediates the security defect, no one else needs to be involved.
- You will see both a production and a branch based review environment both running at the same time and will see the teardown of the branch environment upon merge (with “delete” option for the branch).
- **Any third party security scanner can be integrated** into GitLab and have its results used in Security Policy approval rules.

## Exercises

### Blocking Merges With Security Policies

We will point the security policies configuration back to our same project where a preconfigured rule makes things quicker. In production, this would generally be pointed to a project that contained only security policies and could only be modified by the Security team in your organization.

1. The Tutorial [Terraform Deployment to AWS with GitLab IaC SAST Scanning](TUTORIAL.md) must be completed first so that the project is in the correct state for the following to work. Throughout these exercises this project will be represented as `full/path/to/yourgroup/terraform-web-server-cluster`

2. While browsing `full/path/to/yourgroup/terraform-web-server-cluster`, **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

3. Next to *Approval is optional*, **Click** {the section expand down arrow}

   > Notice: All approval rules say ‘Optional’ in the *Approvals* column.

4. Next to *Security scanning detected…* , **Click** {the section expand down arrow}

5. There should be multiple Criticals for SAST titled ‘Security groups allow ingress from 0.0.0.0:0 and/or ::/0’

   > You will eventually resolve one of these vulnerabilities.

6. On the {left navigation}, ***Click** ‘Settings => Merge requests’ (**NOT** “Code => Merge Requests”)

7. Quite a ways down the page, under *Approvel settings*, if not already done, **Deselect** ‘Prevent approval by author’ 

8. If it is selected, **Deselect** ‘Prevent approvals by users who add commits’

   > `[ONLY FOR LEARNING]` We are doing this ONLY so that we do not need to involve a second GitLab user to demonstrate security policy functionality.

9. Under the section *Approval settings*, **Click** ‘Save changes’ (button) **YOU MUST click the Save changes button immediately under the Approval settings section**

10. On the {left navigation}, ***Click** ‘Secure => Policies’

11. On the *Upper right*, **Click** ‘Edit policy project’

    > Note: You must be a project owner for this button to be visible - if you personally did all previous steps, you should be a project owner of this project.

    > Note: You will be pointing this back to the project you are currently in.

12. On the popup window, under *Select security project*, **Click** ‘Select a project’

13. In the pop field *Search your projects*, **Type** ‘terraform web server cluster’ (if you changed the name of your project, use the actual name you used)

    > Note - you can hover the names in the filtered list to see their full path and name to ensure you click the correct one (the one you are in now).

14. In {the selection list}, **Click** {the current project} (if more than one is listed, be careful to select the current project)

15. **Click** ‘Save’

16. The policies list should update with a new policy named ‘Three or less Criticals for SAST’

    > `[ONLY FOR LEARNING]` While we have presaged this rule and placed it in the same project, a single GitLab project can house Merge Request Security Policy Rules for many projects. This enables policy based management and ensures developers in a project do not have direct permissions to change the policies they must comply with. These policies can also be configured on an entire group hierarchy.

### Researching and Resolving Vulnerability To Enable Merging

> Remembering that the developer would have introduced the vulnerabilities in the MR code (rather than having them prestaged). That means that at this point they could either ask for approval from a security approver who can approve critical vulnerabilities - or they can eliminate the vulnerability that they accidentally added to the code.

1. On the {left navigation}, **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

   > Note: Now the approvals line read “Requires 1 approval from Three or less Criticals for SAST”

2. Next to *Requires 1 approval from Three or less Criticals for SAST*, **Click** {the section expand down arrow}

   > You are blocked from merging this code until at least one critical SAST finding is resolved. If you resolve it, you can merge without anyone else’s participation. Remember that in the real world we would have been the author of this vulnerability by some kind of code change we made in this Dev branch + Merge Request.

   > `[ONLY FOR LEARNING]` - normally you would not want the MR author to be able to approve, especially on security policies, but for learning you will likely see only your avatar for approvers.

3. Next to *Security scanning detected…* , **Click** {the section expand down arrow}

   ![image-20230914052959403](images/MRApprovals.png)

4. Under *SAST findings*, locate the first *Security groups allow ingress from 0.0.0.0:0 and/or ::/0* and **Click** the first ‘Security groups allow ingress from 0.0.0.0:0 and/or ::/0’

5. Next to *Identifiers*, **Click** ‘Unrestricted Security Group Ingress’

   Note: A new page opens on a new tab.

   > The [linked page](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group.html) explains the use of the Terraform resource aws_security_group.

6. Close the current tab and switch back to the last browser tab where the Merge Request view is.

7. In the vulnerability pop-up dialog, next to *File*, **Click** ‘main.tf:159’ (159 is the line number and might be slightly different for you)

   > A new browser tab opens to a resource block titled “aws_security_group_rule” in main.tf so that you are able to locate the specific location where the code needs to be updated.

8. Close the current tab and switch back to the last browser tab where the Merge Request view is.

9. In the vulnerability pop-up dialog, to close the popup, **Click** {the “x” in the upper right corner}

   > In the next section the security groups for the instances will be tightened up to only listen to and talk to the load balancer, rather than all IP addresses.

10. Near the top right of the Merge Request, find the button *Code* and **Click** {its expand down arrow} and **Click** ‘Open in Web IDE’

    **Note**: A new tab opens with VS Code editing a copy of your project.

11. In *The Web IDE*, on {the left side file navigation}, **Click** `main.tf’

12. Find the text `resource "aws_security_group_rule" "ingress"`

13. Within the block locate and **comment** the line starting with `cidr_blocks` with the `#` character.

14. **Uncomment** the next line which starts with `source_security_group_id =`

    Final result should look like this (fragment depicted):

    ```yaml
    resource "aws_security_group_rule" "ingress" {
      type              = "ingress"
      from_port         = var.server_port
      to_port           = var.server_port
      protocol          = "tcp"
      security_group_id = aws_security_group.instance.id
      #cidr_blocks       = ["0.0.0.0/0"]
      source_security_group_id = aws_security_group.elb.id
    }
    ```

    > This change shifts this rule to lock the ELB security group instead of allowing any IP address.

15. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines)

16. In *Commit message*, **Type** ‘Tighten up open ingress’

17. **Click** ‘Commit to ’1-updates’‘

18. Close the current IDE tab and switch back to the last browser tab where the Merge Request view is.

19. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/terraform-web-server-cluster` and **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

20. Refresh the browser tab for updated statuses.

21. Near the top, next to *Merge request pipeline #1111111111111 running*, watch the job status bubbles,

    > The “Pipelines” tab is faster to update, but if you visit it, don’t forget to switch back to the ‘Overview’ tab before continuing.

22. Wait For all jobs to complete successfully (Pipeline Status will be ‘Passed’). (You may need to refresh your browser occasionally)

23. On the Merge Request ‘Overview’ tab, refresh the page.

24. After all jobs have finished, the Approval line should now read `Approval is optional`, **Click** {the section expand down arrow}

    ![image-20230914134102631](images/mrunblocked.png)

25. Notice: that for the rule *Three or less Criticals for SAST*, the *Approvals* column says ‘Optional’

    > The “Three or less Criticals for SAST” rule will now be optional because the MR is below the Critical findings threshold of four or less and so meet the companies security policy for new code vulnerabilities.

    > `[ONLY FOR LEARNING]` To save time in this tutorial, we do not eliminate all Criticals.

26. Next to *Security scanning detected…* , **Click** {the section expand down arrow}

    > Notice that one of the ‘Security groups allow ingress from 0.0.0.0:0 and/or ::/0’ vulnerabilities has disappeared.

27. Browse the activity log for a notice like this screenshot:

    ![image-20230915091035035](images/securitybotnotice.png)

    > Observation: In the next set of steps you will learn that the security vulnerability you just resolved would have normally been something you had accidentally added to your code. The Security Policy Merge Approval Rules just encouraged you to resolve an introduced vulnerability. Your only other recourse would have been to approach the security team to make an exception by approving your merge without removing the vulnerability.

### Testing Production Deployment

1. While in a browser tab on `full/path/to/yourgroup/terraform-web-server-cluster`

2. **Click** ‘Operate => Environments’

3. To the right of *prod-{number}* **Click** ‘Open’ (button)

   > Note: If the environment does not exist yet or you get a page error, the ASG may still be spinning up the environment.

   > Notice the top of the page says ‘Environment: ‘ followed by an environment identifier and that the page has the green color

4. While watching the value for “page retrieved from load balanced instance: ” refresh the page several times to see it shift between instances.

5. Close the tab showing the environment web page.

### Merging To See Review Environment Removed

1. While browsing `full/path/to/yourgroup/terraform-web-server-cluster`, **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

2. Under *Ready to merge!* **Select** ‘Delete source branch’ (default)

3. **Click** “Merge” (blue button)

4. Near the top of the page body, notice that just one of the stages in “Merge request pipeline” is processing.

   > This is the cleanup job removing the DevOps Review Environment for our branch and merge request.

5. Refresh the page until that job is complete.

6. On the left navigation, **Click** ‘Build => Pipelines’

   > This pipeline will deploy to the prod environment because it on the default branch for the repository.

7. Wait For all jobs to complete successfully (Pipeline Status will be ‘Passed’). (You may need to refresh your browser occasionally)

   > Optional: If you have access to the AWS console and deployed to us-east-1, you can use these links to see your new stack resources as well as the removal of the ones prepended by `1-updates`. https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#Instances, https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#SecurityGroups,https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#AutoScalingGroups,https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#LoadBalancers

8. On the left navigation, **Click** ‘Operate => Environments’

   > Notice that the environment ‘review/1-Updates’ is gone and there is a new environment called ‘prod-{number}’

9. To the right of *prod* **Click** ‘Open’

   > Notice the top of the page says ‘Environment: prod-{number}’ and the color change is made in production.

10. Close the current tab and switch back to the last browser tab where the Environment view is.

### Use Security Dashboards To See Vulnerabilities in Default Branch (and Production Environment) and Dependencies SBOM and Licensing SBOM

This section shows some of the GitLab Security Dashboards. These dashboards track the vulnerabilities that are in the code on the default branch. They may be there because new CVEs have been issued for vulnerabilities that were not known at the last time you did a build or because you chose to allow them into the software according to your security policies about vulnerability management.

GitLab Security Dashboards are roughly equivalent to traditional security tool scanning that happens on the entire code base (NOT shifted left like the MR Developer Experience). This capability is needed for a wholistic approach to vulnerability management.

1. While in a browser tab on `full/path/to/yourgroup/terraform-web-server-cluster`

2. **Click** ‘Secure => Vulnerability report’

   **NOTE: Each group level above your current project also has the Vulnerability report dashboard and it shows vulnerabilities for ALL child projects to enable team and organization level management of vulnerabilities.**

   > These vulnerabilities were accepted into the default branch and prod by the Merge Approval process we just completed. This report allows the creation of issues to pay down security debt in future sprints. Dependency vulnerabilities are listed here.
   >
   > To learn more see the [Vulnerability report documentation](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/).

3. **Click** ‘Secure => Dependencies’

   **NOTE: Each group level above your current project also has the Dependency list dashboard and it shows Dependencies for ALL child projects to enable team and organization level management of dependencies.**

   > This will normally show a complete list of the dependencies in your software - which is part of the Software Bill of Materials (SBOM) and it also notes which ones are vulnerable.
   >
   > To learn more see the [Dependency list documentation](https://docs.gitlab.com/ee/user/application_security/dependency_list/).

4. **Click** ‘Secure => License compliance’

   > This will normally show a complete list of licenses in your software - also part of the Software Bill of Materials (SBOM). If we had license policies configured, non-compliant licenses would be flagged. This helps developers notice when licenses of dependencies change from acceptable licenses to undesirable licenses.
   >
   > To learn more see the [License list documentation](https://docs.gitlab.com/ee/user/compliance/license_list.html).

5. **Click** ‘Secure => On-demand scans’

   > This is where you setup scanning of code bases that do not get built very often - so that you can find new vulnerabilities in code that changes infrequently.

   > IMPORTANT: The vulnerabilities that are now in the Security Dashboard will not show in any Merge Requests (including in the next lab) because Merge Requests only show vulnerabilities for code you change in your branch. Our first lab was non-real world in that we purposely created a Merge Request as if we had just created all the code in the project so that we could see the extent of scanning available.

### Create New Merge Request To Understand Per-Dev Branch Managed DevOps Environments and Differential Branch Vulnerabilities in MRs

In this section you will observe that a change in background color is visible in your new MR review environment and then upon merge your review environment is destroyed and the production environment is updated.

1. While in a browser tab on `full/path/to/yourgroup/terraform-web-server-cluster`

2. **Click** ‘Plan => Issues => New issue’ (button)

3. On *New Issue*, for *Title (required)*, **Type** ‘New Color’

4. **Click** ‘Create issue’

5. On *New Color {new issue view}*, **Click** ‘Create merge request’ 

6. On *New merge request*, **Uncheck** ‘Mark as draft’

7. **Click** ‘Create merge request’

8. On *Resolve “New Color” {new merge request view}*

9. In the upper right, find the button *Code* and **Click** {its expand down arrow} and **Click** ‘Open in Web IDE’

   > **Note**: A new tab opens with VS Code editing a copy of your project.

10. On the left navigation, **Click** ‘user_data.sh’

11. **Locate** ‘<body bgcolor="lightgreen">’

12. Change `lightgreen` to `salmon`

13. It should look like this:

    ```html
    <body bgcolor="salmon"> 
    ```

    > **NOTE**: We will now reverse the security fix in our branch to see what happens when a security finding is accidentally introduced in new code. 
    >
    > **Imagine that the code we are changing was actually imported from an AI code suggestion engine.**

14. On {the left side file navigation}, **Click** ‘main.tf’

15. Find the text `resource "aws_security_group_rule" "ingress"`

16. Within the block locate and **UNcomment** the line starting with `cidr_blocks`

17. Use a ``#`` to **Comment** the next line which starts with `source_security_group_id =`

    Final result should look like this, but with a different arn value (fragment depicted):

    ```yaml
    resource "aws_security_group_rule" "ingress" {
      type              = "ingress"
      from_port         = var.server_port
      to_port           = var.server_port
      protocol          = "tcp"
      security_group_id = aws_security_group.instance.id
      cidr_blocks       = ["0.0.0.0/0"]
      #source_security_group_id = aws_security_group.elb.id
    }
    ```

18. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines)

19. In *Commit message*, **Type** ‘Change color’

20. **Click** ‘Commit to ’2-new-color’

21. Close the current tab and switch back to the last browser tab where the Merge Request view is.

22. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/terraform-web-server-cluster` and **Click** ‘Code => Merge requests => Resolve “New Color” ’ 

23. Watch the pipeline status at the top of the Overview section. It starts with “Merge request pipeline #111111111 running”

24. Wait For all jobs in the new pipeline to complete successfully (Pipeline Status will be ‘Passed’).

25. Near the top, under the MR title, **Click** ‘Overview’ ( a tab)

26. Refresh the page.

    > **Empowered Shift Left:** Notice that only new vulnerabilities accidentally added during the code changes are listed. If we had a more traditional security policy rule of zero criticals - merging would be blocked.

    ![image-20230915101556586](images/newvuln.png)

27. **Click** ‘Operate => Environments’

    > Notice you now have one environment for production and one for your current merge request / development branch.

28. You just recently saw that your development environment has a salmon background.

29. To the right of *prod-{number}* **Click** ‘Open’

    > Notice the top of the page says ‘Environment: *prod-{number}’ and the background is white.

30. On the environments list ext to ‘review/2-new-color’ **Click** open to compare.

    > The project now has two completely isolated DevOps environments. One is for your development branch. This enables early detection of DAST, API Fuzzing and other new vulnerabilities while working on a feature branch and requires that they be within policy before being allowed to merge to any shared branch.

    > Now you will merge to the default branch to see the destruction of the Managed DevOps Environment for your MR branch and that your changes also update the Managed DevOps Environment for production.

31. Close the tabs for the environments and switch back to the tab with the Merge Request.

32. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/terraform-web-server-cluster` and **Click** ‘Code => Merge requests => Resolve “New Color” ’

33. Next to *Security Scanning detected…* **Click** the down arrow to expand.

    > **Notice Delta Vulnerabilities Only:** this is the one vulnerability you re-added to your code. The other vulnerabilities that we left in for the last merge (that now appear on the security dashboard) are not shown because you did not introduce them in your code on this branch.

34. Right before *Requires 1 approval from Three or less Criticals for SAST*  **Click** ‘Approve’ (blue button)

35. Under *Ready to merge!*, **Click**, ‘Merge’ (blue button)

36. On the left **Click** ‘Build => Pipelines’

    > The MR pipeline will be running one job to destroy the review environment and a new Merge Pipeline will be running on the default branch.

37. Wait for the Pipelines to complete, Refresh the screen periodically.

38. Directly under where you pressed the button, you can see the status of the pipeline merging to production, wait for it to complete.

39. **Click** ‘Operate => Environments’

    > Notice that the review environment is gone.

40. To the right of *prod-{number}* **Click** ‘Open’ to observe the review branch color change is now in production. (The ASG may take some time to replace instances with the new color)

    > Optional: If you have access to the AWS console and deployed to us-east-1, you can use these links to see your new stack resources as well as the removal of the ones prepended by `1-updates`. The new ones should start with `prod`: https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#Instances, https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#SecurityGroups,https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#AutoScalingGroups,https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#LoadBalancers

### What Was Observed And Learned

Review “Concepts to Watch for” and reflect on how each of these items was seen in the lab work.

