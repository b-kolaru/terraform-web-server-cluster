# Terraform Deployment to AWS with GitLab IaC SAST Scanning

## Known Working Version Details

Tested Date: **2023-08-08**

Testing Version (GitLab and Runner): **Enterprise Edition 16.4.0 SaaS**

Report Problems with New Issues here: https://gitlab.com/guided-explorations/aws/terraform/terraform-web-server-cluster/-/issues

There is nothing here that depends on GitLab.com, so it should work fine on other Gitlab instances that are licensed for Ultimate.

**IMPORTANT:** Requirements for self-paced execution of this tutorial are covered in [TUTORIAL-Requirements.md](TUTORIAL-Requirements.md)

## Visual Overview

![terraformtutorial1overview](images/terraformtutorial1overview.png)

## Conventions and Requirements

- Frequently a click in a browser application will launch a new tab. The exercise instructions will also frequently ask you to return to the tab that launched a new tab.  It can be challenging to keep your context when web applications open new tabs so the exercises generally highlight when a new tab has been opened. Keep this in mind as you work through and do not be quick to clean up open tabs.
- `[ONLY FOR LEARNING]` = This tag means that for the sake of learning simplicity we doing or showing something that would not be typical in a production environment.
- The labs do not direct you to watch the creation and destruction of the AWS resources - but you can feel free to watch these events to whatever degree you are familiar with the AWS console.

> Callouts like these will be used to note unique learning points about the steps just completed. 

## Concepts To Watch For

- Many CI systems use ‘build failure’ on shared branches as the primary or exclusive way to force software defects to be dealt with. 
  - There are a number of downsides to this approach if other controls are possible - especially with security vulnerability defects. For instance, having to wait on assistance before any more work can be done. Also, the pain of one-by-one-failures discovery of vulnerabilities.
  - GitLab has very flexible approvals - some of which only become required under the right conditions. Since GitLabs Merge Request process supports this for security vulnerabilities, it prevents a the very disruptive workflow of failing pipelines on vulnerabilities, but still does not allow them to be merged into the next branch. This maximizes developer productivity.
- GitLab’s empowered shift left maximizes the ‘in-context’ help for developers to handle vulnerabilities and it is focused on the code changes in the developers Merge Request / Feature branch so that there is clear responsibility for the introduction of defects right as they happen.
- The SAST scanning results are Terraform and AWS specific due to using GitLab Kics IaC SAST and third party scanners\.

## Exercises

### Creating GitLab Group and Project

1. Start by creating a new project in the GitLab group of your choosing on the GitLab instance of your choosing
   Throughout these exercises this group will be represented as `full/path/to/yourgroup`.

2. Use the GitLab UI to **navigate to** ‘full/path/to/yourgroup’

3. On the left navigation, **Click** ‘Group overview’

4. Near the bottom right of the page, **Click** ‘Create new project’ (Large panel)

5. On the *Create new project* page, **Click** ‘Import project’ (Large panel)

6. On the *Import project* page, **Click** ‘Repository by URL’ (upper right button)

   > Note: Page expands into the Repository by URL form.

7. Find field *Git repository URL*, **Type or Paste** https://gitlab.com/guided-explorations/aws/terraform/terraform-web-server-cluster.git

   > Throughout these exercises this project will be represented as `full/path/to/yourgroup/terraform-web-server-cluster`

8. **Click** ‘Create project’ (bottom left button)

   > Note: The Project overview page will display for your new project.

### Configuring GitLab CI Variables for AWS Keys

The IAM user whose keys you specify here should have administrator permissions to the region where you wish to deploy the stack.

1. Open a browser tab to `full/path/to/yourgroup/terraform-web-server-cluster`

2. On the left navigation, **Click** ‘Settings => CI/CD’

3. To the right of *Variables*, **Click** ‘Expand’

4. Use **Add variable** once for each table row and specify the variables settings as indicated in the table. 

   | Key                   | Value                                                        | Protect<br />(IMPORTANT!) | Mask |
   | --------------------- | ------------------------------------------------------------ | ------------------------- | ---- |
   | AWS_ACCESS_KEY_ID     | Copy from “Access key” in IAM Keys page left open earlier    | No                        | No   |
   | AWS_SECRET_ACCESS_KEY | Copy from “Secret access key” in IAM Keys page left open earlier | No                        | Yes  |
   | AWS_DEFAULT_REGION    | us-east-1 (or another region you have access to)             | No                        | No   |

### Use Merge Request Single Source of Truth To See Security Findings

In this section you will create a GitLab Issue and Merge Request. It is important to work on code using a Merge Request so that we can see all defects associate with just the code we’ve changed on our branch - including security vulnerabilities - which are also ONLY for code we’ve changed or added.

`[ONLY FOR LEARNING]`Since we’ve never run a pipeline on a the default branch, all existing security vulnerabilities will appear in the MR as if we just created them all in our branch. This works well for ensuring training exercises are shorter because presaged vulnerabilities show up in MRs, but in the real world, all vulnerabilities already on the default branch would not be shown - only vulnerabilities that we introduced with our MR code changes. The fact that MR Vulnerability Findings are from our code changes dramatically shifts the responsibility and desire to eliminate them like all other code defects.

1. While in a browser tab on `full/path/to/yourgroup/terraform-web-server-cluster`

2. **Click** ‘Plan => Issues => New issue’ (button)

3. On *New Issue*, for *Title (required)*, **Type** ‘Updates’

4. **Click** ‘Create issue’

5. On *Updates {new issue view}*, **Click** ‘Create merge request’ 

6. On *New merge request*, **Uncheck** ‘Mark as draft’

7. **Click** ‘Create merge request’

   > **Merge Requests - Developer’s Single Source of Truth and Collaboration**
   > GitLab’s work flow from Issue to Merge Request automatically creates a branch to work on. By initiating this workflow from the issue, there is immediate visibility that work has started and all the artifacts are cleanly linked together. The branch and merge request are already associated and the issue will be closed upon successfully merging. The Merge Request is also the Single Source of Truth (SSOT) for all changes and change approvals for the developer and all collaborators and approvers.
   >
   > When enabled, Merge Requests are also capable of creating an isolated DevOps environment for the code changes - which enables all kinds of CI checks that require a running version of the application, including: DAST Scanning, API Fuzzing, Accessibility Testing, Performance Testing, Browser Testing and any Human QA that is not yet automated.

8. On *Resolve “Updates” {new merge request view}*

9. Just under the title *Resolve “Updates”* **Click** ‘Pipelines’

   > Notice tha a pipeline to deploy an isolated environment for your Merge Request has already started to run.

10. Just under the title *Resolve “Updates”* **Click** ‘Overview’

11. In the upper right of the page, find *Code* (button) and **Click** {its expand down arrow} and **Click** ‘Open in Web IDE’

    **Note**: A new tab opens with VS Code editing a copy of your project.

    > GitLab supports multiple hosted code editing environments. The WebIDE is a VS Code based editor for all your files. GitPod enables developer environments along with the IDE environment. Single file editing is also available for quick changes.

12. On the left navigation, **Click** ‘user_data.sh’

13. **Locate** `<body BGCOLOR="white">`

14. Change `white` to `lightgreen`

15. It should look like this:

    ```html
    <body bgcolor="lightgreen"> 
    ```

16. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines)

17. In *Commit message*, **Type** ‘Update color’

18. **Click** ‘Commit to ’1-updates’‘

    > When working in GitLab’s integrated editing environments, creating a commit is also equivalent to doing a Git push operation, which will also trigger CI checks when CI is enabled.

19. Close the IDE tab.

20. **Click** {the Merge Request tab from which the IDE was launched}

21. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/terraform-web-server-cluster` and **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

22. Near the top, under the MR title, **Click** ‘Pipelines’ ( a tab)

    > There should be two pipelines running - one from when the branch was created by the merge request creation process and one for your most recent change.

23. Wait For all jobs to complete successfully (Pipeline Status will be ‘Passed’) - refresh your screen to get updated status. It may be necessary to visit “Build => Pipelines” for the most accurate pipeline status.

    > If all jobs on the most recent pipeline do not complete successfully, there may have been a conflict between two pipelines, if this happens, **Click** ‘Run pipeline’ (a button)

    > Optional: If you have access to the AWS console and deployed to us-east-1, you can use these links to see your new stack resources. They should start with `1-updates`: https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#Instances, https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#SecurityGroups,https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#AutoScalingGroups,https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#LoadBalancers

24. On the left navigation, **Click** ‘Secure => Security configuration

25. On the *Security Configuration* page, **Click** ‘Vulnerability Management’ (last click is a tab under the page heading)

26. On *Security configuration*, under *Security training*, **Click** each of the {slider buttons next to ‘Kontra’, ‘Secure Code Warrior’, ‘SecureFlag’}

27. On the left navigation, **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

    > `[ONLY FOR LEARNING]` As mentioned before, Merge Requests generally only contain the results of CI checks for code that has been changed in the Merge Request itself. So all defects and vulnerabilities are for code the developer has just changed. In this case there are presaged vulnerabilities and since our very first pipeline is for a new merge request, it looks as if we created the entire code base in our Merge Request and therefore serves to show all kinds of findings from GitLab scanning. In real world setups the developer is very focused on the findings because they pertain only to changes they just made.

28. Near the top, under the MR title, **Click** ‘Overview’ ( a tab)

29. There should be expandable sections for “License Compliance” and “Security scanning detected…” - if they are not visible or not yet able to be expanded, keep refreshing your browser until the sections are completely populated and expandable.

### Examining Finds in MR

1. Use this screenshot to explore the security and SBOM MR Widget content:

   ![mr1](images/mr1.png)

2. Click on of the findings titled ‘Improper limitation of a pathname to a restricted directory (’Path Traversal’)’ and use this screenshot to explore: 

   ![image-20230913073337821](images/mr2.png)

### Testing Deployed MR Isolated DevOps Environment

1. While in a browser tab on `full/path/to/yourgroup/terraform-web-server-cluster`

2. **Click** ‘Operate => Environments’

3. To the right of *review/1-updates* **Click** ‘Open’ (button)

   > Notice the top of the page says ‘Environment: ‘ followed by an environment identifier.

4. The background should be light green. 

   1. All pipelines must be complete for the change to be evident AND
   2. The Autoscaling group needs time to replace instances - so keep refreshing.
   3. If you catch the ASG mid change you may even see the page alternate between the old and new color.

5. Once the color has changed, keep refreshing to see the page change between the two instances hosting the site.

> Optional: If you have access to the AWS console and deployed to us-east-1, you can use these links to see your new stack resources: https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#Instances, https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#SecurityGroups,https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#AutoScalingGroups,https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#LoadBalancers

### What Was Observed And Learned

Review “Concepts to Watch for” and reflect on how each of these items was seen in the lab work.
In addition to security findings you are seeing viewing an isolated environment just for this merge request.

### Security Policy Approval Rules and Managed DevOps Environments Tutorial

Explore how GitLab Security Policy rules can require special merge approvals for a wide variety of vulnerability criteria by moving on to: [Secure Serverless Framework Development with Security Policy Approval Rules and Managed DevOps Environments](